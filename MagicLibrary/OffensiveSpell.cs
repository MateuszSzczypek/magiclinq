﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagicLibrary
{
    public class OffensiveSpell : Spell
    {
        public int Damage { get; set; }

        public override string ToString()
        {
            return string.Format("\t{0}, DMG:{1}\n", base.ToString(), Damage);
        }
    }
}
