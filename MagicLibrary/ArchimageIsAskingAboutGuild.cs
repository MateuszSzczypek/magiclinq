﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagicLibrary
{
    public partial class MagiGuild
    {
        public void AllMagiByName()
        {
            var sortedMagi =
                from mag in Magi
                orderby mag.Name
                select mag;

            foreach (var posortowany in sortedMagi)
            {
                Console.WriteLine(posortowany.ToString());
            }
        }

        public void AllExpedMagi(int poziom)
        {
            var sortedMagi =
                from mag in Magi
                where mag.Level > poziom
                orderby mag.Name ascending
                select mag;

            foreach (var posortowany in sortedMagi)
            {
                Console.WriteLine(posortowany.ToString());
            }
        }


        public void AllTheBestMagi(int poziom)
        {
            var sortedMagi =
                from mag in Magi
                where mag.Inteligence > 20 && mag.Level <= poziom
                orderby mag.Inteligence descending
                select mag;

            foreach (var posortowany in sortedMagi)
            {
                Console.WriteLine(posortowany.ToString());
            }
        }

        public void AllOffensiveMagi()
        {
            var sumMP =
                (from mag in Magi
                where mag.Level > 2 && mag.Dextirity > 10
                select mag.MP).Sum();

             Console.WriteLine("Suma magii wszystkich magow o lvl>2 i zrecznosci > 10 wynosi:"+sumMP);
        }

        public void TheMostNumberOfSpells(int ileCzarow)
        {
            var sortedMagi =
                from mag in Magi
                where mag.SpellBook.Count >= ileCzarow
                orderby mag.SpellBook.Count descending
                select new { mag.Name, mag.SpellBook.Count, mag.MP};

            foreach (var posortowany in sortedMagi)
            {
                Console.WriteLine(posortowany.ToString());
            }
        }

        
        public void TheMostComprehensiveMagi(int poziom)
        {
            var sortedMagi =
                Magi.Select (mag => new { mag.Name, mag.Level, Average = ((mag.Dextirity + mag.Strength + mag.Inteligence) / 3) })
                 .OrderByDescending(mag => mag.Average);

            foreach (var posortowany in sortedMagi)
            {
                Console.WriteLine(posortowany);
            }
        }
        
        public void WhoHasTheMostSpells()
        {
            var sortedMagi =
                Magi.Where( mag => mag.SpellBook.Count == (Magi.Max( maks => maks.SpellBook.Count)))
                .Select(mag => mag);
            
            foreach (var posortowany in sortedMagi)
            {
                Console.WriteLine(posortowany.ToString());
            }
        }

        
        public void WhatUniqueSpellsYouHave()
        {
            var sortedMagi =
                (from mag in Magi
                 from czary in mag.SpellBook
                select czary).Distinct();

            foreach (var posortowany in sortedMagi)
            {
                Console.WriteLine(posortowany.ToString());
            }
        }

        public void AllSpellsOfSpecifiedKind(Spell spell)
        {
               var sortedMagi =
                (Magi.SelectMany(mag =>  mag.SpellBook )
                 .Where(czar => czar.GetType().Equals(spell.GetType()))).Distinct();

            foreach (var posortowany in sortedMagi)
            {
                Console.WriteLine(posortowany.ToString());
            }
        }
        
        public void OffensiveSpellsOfMage(string name)
        {
            var sortedMagi =
                (from mag in Magi
                where mag.Name.Equals(name)
                select mag).Single();
            
            Console.WriteLine(sortedMagi);
        }

        public void HowManySpellsOfAnyType()
        {
            var sortedMagi =
               Magi
               .GroupBy(czar => new { czar.SpellBook })
               .Select(g => new { g.Key.SpellBook, g }).SelectMany(mag => mag.SpellBook);

            foreach (var posortowany in sortedMagi)
            {
                Console.WriteLine(posortowany);
            }
        }
        
        public void HowManySpellsOfAnyTypeInOneMage(string imie)
        {
            var sortedMagi =
               Magi
               .Where( ludz => ludz.Name == imie)
               .GroupBy(czar => new { czar.SpellBook })
               .Select(g => new { g.Key.SpellBook, g }).SelectMany(mag => mag.SpellBook);

            foreach (var posortowany in sortedMagi)
            {
                Console.WriteLine(posortowany);
            }
        }
        
        public void TheStrongestOnes(string imie)
        {
            var sortedMagi =
                Magi.Select(mag => new { mag.Name, mag.Level, mag.Experience })
                 .OrderByDescending(mag => mag.Level).ThenByDescending(mag => mag.Experience).Take(3).Skip(1);

            foreach (var posortowany in sortedMagi)
            {
                Console.WriteLine(posortowany.ToString());
            }
        }

        public Boolean FullHPAndMP(string imie)
        {
            var sortedMagi =
                (from mag in Magi
                 select mag).All(m => ((m.HP == m.MaxHP) && (m.MP == m.MaxMP)));

            return sortedMagi;
        }

        public Boolean DoesAnyoneFaint()
        {
            var sortedMagi =
                (from mag in Magi
                 where (mag.HP == mag.MaxHP) && (mag.MP == mag.MaxMP)
                 select mag).Any(m => m.HP == 0);
            
            return sortedMagi;
        }
        
        public void SpecjalMission()
        {
            var sortedMagi =
                Magi
                .Select(mag => new {
                    mag.Name,
                    mag.Level,
                    mag.PhysicalRes,
                    mag.PoisonRes,
                    mag.IceRes,
                    mag.FireRes,
                    Suma = (Magi.Sum( sumka => mag.PhysicalRes + mag.PoisonRes + mag.IceRes + mag.FireRes))/Magi.Count })
                 .OrderByDescending(mag => mag.Suma);
            
            foreach (var posortowany in sortedMagi)
            {
                Console.WriteLine(posortowany.ToString());
            }
        }
    }
}
