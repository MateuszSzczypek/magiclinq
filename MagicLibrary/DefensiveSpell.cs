﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagicLibrary
{
    public class DefensiveSpell : Spell
    {
        public int Protection { get; set; }

        public override string ToString()
        {
            return string.Format("\t{0}, Def:{1}\n", base.ToString(), Protection);
        }
    }
}
