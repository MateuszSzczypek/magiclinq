﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagicLibrary
{
    public abstract class Spell
    {
        public string Name { get; set; }
        public int ManaCost { get; set; }
        public int Cooldown { get; set; }

        public override string ToString()
        {
            return string.Format("\t{0}, MC:{1}, CD:{2}", Name, ManaCost, Cooldown);
        }

        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }  
        public override bool Equals(object obj)
        {
            return ToString() == obj.ToString();
        }
    }
}
