﻿using MagicLibrary.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagicLibrary
{
    public delegate void MageEventHandler(Mage mage);
    
    public partial class MagiGuild
    {
        public event MageEventHandler OnAcceptedMage;
        public event MageEventHandler OnKickOutMage;
        public List<Mage> Magi { get; set; }
        

        public MagiGuild()
        {
            Magi = new List<Mage>();
        }

        public override string ToString()
        {
            string tmp = "";
            foreach (Mage mag in Magi)
            {
                tmp += string.Format("{0}", mag);
            }
            return tmp;
        }

        public void AcceptMage(Mage newMage)
        {
            Magi.Add(newMage);
            if(OnAcceptedMage != null)
            {
                OnAcceptedMage(newMage);
            }
        }

        public void KickMage(string imieMaga)
        {
            foreach (Mage magu in Magi)
            {
                if (magu.Name.Equals(imieMaga))
                {
                    Magi.Remove(magu);
                    OnKickOutMage(magu);
                    break;
                }
                else
                {
                    throw new MageNotFoundException("Nie ma takiego maga w gildii...");
                }
            }
        }

    }

    
}
