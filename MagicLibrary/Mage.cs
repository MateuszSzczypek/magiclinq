﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagicLibrary
{
    public class Mage : IComparable
    {
        public string Name { get; set; }
        public int Level { get; set; }
        public int Experience { get; set; }
        public int Strength { get; set; }
        public int Dextirity { get; set; }
        public int Inteligence { get; set; }
        public int HP { get; set; }
        public int MaxHP { get; set; }
        public int MP { get; set; }
        public int MaxMP { get; set; }
        public int Damage { get; set; }
        public int PhysicalRes { get; set; }
        public int FireRes { get; set; }
        public int IceRes { get; set; }
        public int PoisonRes { get; set; }

        public SpellBook SpellBook { get; set; }

        public override string ToString()
        {
            return string.Format("{0}, Hp {1}/{2}, MP {3}/{4}, Lvl {5}, Exp {6}, Str {7}, Dex {8}, Int {9}, Dmg {10}, Res [Physical {11}, FIre {12}, Ice {13}, Poison {14} \n{15}",
                Name, HP, MaxHP, MP, MaxMP, Level, Experience, Strength, Dextirity, Inteligence, Damage, PhysicalRes, FireRes, IceRes, PoisonRes, SpellBook);
        }

        public int CompareTo(object obj)
        {
            Mage temp = (Mage)obj;
            return this.Strength.CompareTo(temp.Strength);
        }
    }
}