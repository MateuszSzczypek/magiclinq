﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagicLibrary
{
    public class SpellBook : List<Spell>
    {
        public override string ToString()
        {
            string tmp = "";
            foreach (Spell czar in this)
            {
                tmp += string.Format("{0}",czar);
            }
            return tmp;
        }
    }
}
