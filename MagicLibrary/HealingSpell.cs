﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagicLibrary
{
    public class HealingSpell : Spell
    {
        public int Health { get; set; }

        public override string ToString()
        {
            return string.Format("\t{0}, HP:{1}\n", base.ToString(), Health);
        }
    }
}
