﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagicLibrary.Exceptions
{

    [Serializable]
    public class MageNotFoundException : ApplicationException
    {
        public MageNotFoundException() { }
        public MageNotFoundException(string message) : base(message) { }
        public MageNotFoundException(string message, Exception inner) : base(message, inner) { }
        protected MageNotFoundException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context)
        { }
    }
}
