﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagicLibrary
{
    static class MetodyRozszerzeniowe
    {
        public static void OperatoryZapytania(this MagiGuild gildia)
        {
            var sortedMagi =
                from mag in gildia.Magi
                where mag.Strength > 10
                orderby mag.Strength descending
                select mag;

            foreach (var posortowany in sortedMagi)
            {
                Console.WriteLine(posortowany.ToString());
            }
        }

        public static void LINQInstancyjne(this MagiGuild gildia)
        {
            var sortedMagi = gildia.Magi
                .Where (s => s.Strength > 10)
                .OrderByDescending(s => s)
                .Select(s => s);

            foreach (var posortowany in sortedMagi)
            {
                Console.WriteLine(posortowany.ToString());
            }
        }

        public static void LINQRozszerzeniowe(this MagiGuild gildia)
        {
            var sortedMagi = Enumerable.Where(gildia.Magi,
                s => s.Strength > 10)
                .OrderByDescending(s => s)
                .Select(s => s);

            foreach (var posortowany in sortedMagi)
            {
                Console.WriteLine(posortowany.ToString());
            }
        }

        public static void LINQAnonimowe(this MagiGuild gildia)
        {
            Func<Mage, bool> whereFilter =
                delegate (Mage magu) { return magu.Strength > 10; };
            Func<Mage, Mage > mageToProcess =
                delegate (Mage magu) { return magu; };

            var sortedMagi = gildia.Magi
                .Where(whereFilter)
                .OrderByDescending(mageToProcess)
                .Select(mageToProcess);

            foreach (var posortowany in sortedMagi)
            {
                Console.WriteLine(posortowany.ToString());
            }
        }

        public static void LINQMetodowe(this MagiGuild gildia)
        {
            var sortedMagi = gildia.Magi
                .Where(whereFilterMethod)
                .OrderByDescending(mageToProcessMethod)
                .Select(mageToProcessMethod);

            foreach (var posortowany in sortedMagi)
            {
                Console.WriteLine(posortowany.ToString());
            }
        }

        private static Boolean whereFilterMethod(this Mage magu)
        {
            if(magu.Strength > 10)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private static Mage mageToProcessMethod(this Mage magu)
        {
            return magu;
        }

        public static void LINQBezLINQ(this MagiGuild gildia)
        {
            List<Mage> tmpList = new List<Mage>();
            foreach (var posortowany in gildia.Magi)
            {
                if(posortowany.Strength > 10)
                {
                    tmpList.Add(posortowany);
                }
            }
            tmpList.Sort();
            tmpList.Reverse();

            foreach (Mage magik in tmpList)
            {
                Console.WriteLine(magik);
            }
        }
    }
}
