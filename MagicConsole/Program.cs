﻿using MagicLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagicConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            MagiGuild guild = initializeGuild();
            Console.WriteLine(guild);


            //Podpiecie metod do delegatow
            guild.OnAcceptedMage += delegate (Mage mage)
            {
                Console.WriteLine("\nSiema, jestem nowy magu w gildii. Nazywam sie:{0}", mage.Name);
            };

            guild.OnKickOutMage += delegate (Mage mage)
            {
                Console.WriteLine("\nKomunikat gildii: usunieto z gildii:{0}", mage.Name);
            };

            //---------------------------------------------------------------------------------------------------------
            //Testowanie metod z zapytaniami LINQ - Linked Input Querry
            guild.AllExpedMagi(70);
            //guild.AllMagiByName();
            //guild.AllOffensiveMagi();
            //guild.AllTheBestMagi(100);
            //Console.WriteLine("Czy ktos zemdlal?--------->:" + guild.DoesAnyoneFaint());
            //Console.WriteLine("Czy Gandalf ma max HP i MP?--------->:" + guild.FullHPAndMP("Gandalf"));
            //guild.OffensiveSpellsOfMage("Gandalf");
            //guild.TheMostNumberOfSpells(3);
            //guild.TheMostComprehensiveMagi(60);
            //guild.TheStrongestOnes("Gandalf");
            //guild.AllSpellsOfSpecifiedKind(new OffensiveSpell { });
            //guild.WhatUniqueSpellsYouHave();
            //guild.SpecjalMission();
            //guild.WhoHasTheMostSpells();

            //---------------------------------------------------------------------------------------------------------
            //Testowanie metod rozszerzeniowych
            //guild.OperatoryZapytania();
            //guild.LINQInstancyjne();
            //guild.LINQRozszerzeniowe();
            //guild.LINQAnonimowe();
            //guild.LINQMetodowe();
            //guild.LINQBezLINQ();


            //Test delegatow
            guild.AcceptMage(new Mage { Name = "Wladzio" });
            guild.KickMage("Gandalf");

            Console.Read();
        }

        private static MagiGuild initializeGuild()
        {
            MagiGuild guild = new MagiGuild();
            guild.AcceptMage(new Mage
            {
                Name = "Gandalf",
                Level = 100,
                Experience = 7000000,
                Strength = 15,
                Dextirity = 16,
                Inteligence = 21,
                HP = 200,
                MaxHP = 200,
                MP = 400,
                MaxMP = 400,
                Damage = 27,
                PhysicalRes = 50,
                FireRes = 50,
                IceRes = 50,
                PoisonRes = 50,
                SpellBook = new SpellBook
                {
                    new OffensiveSpell  { Name = "Fireball", ManaCost = 25, Cooldown = 10, Damage = 75 },
                    new OffensiveSpell  { Name = "Light Ray", ManaCost = 25, Cooldown = 10, Damage = 225 },
                    new OffensiveSpell  { Name = "Force Push", ManaCost = 25, Cooldown = 10, Damage = 20 },
                    new DefensiveSpell { Name = "Barrier", ManaCost = 20, Cooldown = 15, Protection = 10 },
                    new HealingSpell    { Name = "Mass Heal", ManaCost = 30, Cooldown = 8, Health = 100 }
                }
            });
            guild.AcceptMage(new Mage
            {
                Name = "Harry Potter",
                Level = 50,
                Experience = 1000000,
                Strength = 12,
                Dextirity = 14,
                Inteligence = 17,
                HP = 150,
                MaxHP = 150,
                MP = 250,
                MaxMP = 250,
                Damage = 15,
                PhysicalRes = 10,
                FireRes = 10,
                IceRes = 10,
                PoisonRes = 10,
                SpellBook = new SpellBook
                {
                    new OffensiveSpell  { Name = "Fireball", ManaCost = 25, Cooldown = 10, Damage = 75 },
                    new DefensiveSpell { Name = "Barrier", ManaCost = 20, Cooldown = 15, Protection = 10 },
                }
            });
            return guild;
        }

    }
}
